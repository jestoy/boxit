#!/usr/bin/env python3

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()
